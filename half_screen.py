import pydirectinput
import time
import random


#time to change from console to game
time.sleep(3)

def get_pixel_colour(i_x, i_y):
	import PIL.ImageGrab
	return PIL.ImageGrab.grab().load()[i_x, i_y]

def black_screen():
	while get_pixel_colour(600,90)==(0,0,0):
		time.sleep(0.1)
		print("Waiting for black screen to disappear.")
	
init="43321344214223113141313433343343321113311311431311333133234342332313332321224434331444442343433333422412412323113122444444"
while(True):


	black_screen()

	if get_pixel_colour(500,300)==(153,78,0):
		init = init[:(len(init)-3)]
		print("Starting New Game.")
		time.sleep(1.3)
		g = 0
		pydirectinput.keyUp(',')
		pydirectinput.keyDown('shift')
		pydirectinput.keyDown('enter')
		time.sleep(1)
		pydirectinput.keyUp('enter')

	while get_pixel_colour(500,300)==(153,78,0):
		pydirectinput.keyUp(',')
		pydirectinput.press('shift')
		pydirectinput.press('enter')
		print("Still trying to start new game.")
	
	black_screen()
	
	time.sleep(0.3)
	print("Executing initial successful commands")
	i = 0	
	#pydirectinput.keyDown(',')

	while i < len(init):
		pydirectinput.keyDown(',')
		print(init[i])
		time.sleep(0.4)
		if int(init[i])==1 or int(init[i])==4:
			print("Jump")
			pydirectinput.keyDown('right')
			pydirectinput.keyDown('.')
			time.sleep(0.4)
			pydirectinput.keyUp('.')
			pydirectinput.keyUp('right')
		elif int(init[i])==2:
			print("Right")
			pydirectinput.keyDown('right')
			time.sleep(0.5)
			pydirectinput.keyUp('right')
		elif int(init[i])==3:
			pydirectinput.keyDown('left')
			time.sleep(.2)
			pydirectinput.keyUp('left')
		#elif int(init[i])==4:
		#	pydirectinput.press(',')
		i = i+1


		


	pydirectinput.keyUp(',')
	print("Initial Commands Executed.")
	while get_pixel_colour(600,90)==(0,0,0):
		time.sleep(0.1)
	#RANDOMLY CHOOSE A NEW MOVE
	g = 1
	print("made it 1")
	print("Executing New Random Commands")
	while(g):
		print("made it 2")
		if get_pixel_colour(500,300)==(153,78,0):
			init = init[:(len(init)-3)]
	
			time.sleep(1.3)
			break
			pydirectinput.press('enter')
		print("made it 3")
		if  get_pixel_colour(600,90)==(0,0,0):
			print("SOFT RESET")
			init = init[:(len(init)-3)]
			pydirectinput.keyUp(',')
			pydirectinput.keyDown('shift')
			pydirectinput.keyDown('r')
			time.sleep(0.2)
			pydirectinput.keyUp('shift')
			pydirectinput.keyUp('r')
			break
		time.sleep(0.4)
		print("made it 4")
		pydirectinput.keyDown(',')
		num = random.randint(1,4)
		if num==1 or num==4:
			pydirectinput.keyDown('right')
			pydirectinput.keyDown('.')
			time.sleep(0.4)
			pydirectinput.keyUp('.')
			pydirectinput.keyUp('right')
		elif num==2:
			pydirectinput.keyDown('right')
			time.sleep(.5)
			pydirectinput.keyUp('right')
		elif num==3:
			pydirectinput.keyDown('left')
			time.sleep(.2)
			pydirectinput.keyUp('left')
		init += str(num)
		print(init)


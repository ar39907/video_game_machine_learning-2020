import pydirectinput
import time
import random


#time to change from console to game
time.sleep(2)

def get_pixel_colour(i_x, i_y):
	import PIL.ImageGrab
	return PIL.ImageGrab.grab().load()[i_x, i_y]

def black_screen():
	while get_pixel_colour(600,90)==(0,0,0):
		time.sleep(0.1)
		print("Waiting for black screen to disappear.")
orig_init = 0
tot_round_init = 0
init = []
x = []
for n in range(100):
	init.append("")
	x.append(3)
init_r = ""
d_cnt = 1

j = -1
cnt=0
while(True):
	j+=1

	black_screen()

	if get_pixel_colour(500,300)==(153,78,0):
		tmp = init[j-1]
		print("j: " + init[j])
		print("j+1: " + init[j+1])
		print("j-1: " + init[j-1])
		print("THIS IS BEFORE: " + init[j-1])
		init[j-1] = (tmp[:(len(tmp)-10)])
		print("THIS IS AFTER: " + init[j-1])
		print("Starting New Game.")
		time.sleep(1.3)
		g = 0
		pydirectinput.keyUp(',')
		#pydirectinput.keyDown('shift')
		pydirectinput.keyDown('enter')
		time.sleep(1)
		pydirectinput.keyUp('enter')

	while get_pixel_colour(500,300)==(153,78,0):
		pydirectinput.keyUp(',')
		#pydirectinput.press('shift')
		pydirectinput.press('enter')
		print("Still trying to start new game.")
	
	black_screen()
	
	time.sleep(0.3)
	print("Executing initial successful commands")
	i = 0	
	pydirectinput.keyDown(',')
	k = 0
	while k < cnt:
		while i < len(init[k]):
			tmp_init = init[k]
			print("doing this now: " + tmp_init[i])
			if int(tmp_init[i])<x[k]:
				pydirectinput.keyDown('right')
				pydirectinput.keyDown('.')
				time.sleep(0.4)
				pydirectinput.keyUp('.')
				pydirectinput.keyUp('right')
			elif int(tmp_init[i])>x[k] and int(init[k])<7:
				pydirectinput.keyDown('right')
				time.sleep(1)
				pydirectinput.keyUp('right')
			elif int(tmp_init[i])>=7:
				pydirectinput.press('left')
			#elif int(init[k])<x[k]:
			#	pydirectinput.press(',')
			i = i+1
			print(tmp_init)
			if  get_pixel_colour(600,90)==(0,0,0):
				print("SOFT RESET 1")
				tmp = init[j-1]
				print("I AM DELETING THIS: " + tmp)
				print("THIS IS BEFORE: " + init[j-1])
				init[j-1] = (tmp[:(len(tmp)-10)])
				print("THIS IS AFTER: " + init[j-1])
				pydirectinput.keyUp(',')
				pydirectinput.keyDown('shift')
				pydirectinput.keyDown('r')
				time.sleep(0.2)
				pydirectinput.keyUp('shift')
				pydirectinput.keyUp('r')

		k+=1
	cnt +=1
		


	if  get_pixel_colour(600,90)==(0,0,0):
		print("SOFT RESET 1")
		tmp = init[j-1]
		print("I AM DELETING THIS: " + tmp)
		print("THIS IS BEFORE: " + init[j-1])
		init[j-1] = (tmp[:(len(tmp)-10)])
		print("THIS IS AFTER: " + init[j-1])
		pydirectinput.keyUp(',')
		pydirectinput.keyDown('shift')
		pydirectinput.keyDown('r')
		time.sleep(0.2)
		pydirectinput.keyUp('shift')
		pydirectinput.keyUp('r')
	


	pydirectinput.keyUp(',')
	print("Initial Commands Executed.")
	while get_pixel_colour(600,90)==(0,0,0):
		time.sleep(0.1)
	#RANDOMLY CHOOSE A NEW MOVE
	g = 1

	print("Executing New Random Commands")
	while(g):

		if get_pixel_colour(500,300)==(153,78,0):
			tmp = init[j]
			init[j] = (tmp[:(len(tmp)-10)])
	
			time.sleep(1.3)
			break
			pydirectinput.press('enter')
			
		if  get_pixel_colour(600,90)==(0,0,0):
			d_cnt = d_cnt+1
			print("SOFT RESET 2")
			tmp = init[j-1]
			print("THIS IS BEFORE: " + init[j-1])
			init[j-1] = (tmp[:(len(tmp)-10)])
			print("THIS IS AFTER: " + init[j-1])

			pydirectinput.keyUp(',')
			pydirectinput.keyDown('shift')
			pydirectinput.keyDown('r')
			time.sleep(0.2)
			pydirectinput.keyUp('shift')
			pydirectinput.keyUp('r')
			break

		if d_cnt%10==0:
			
			if x>6:
				x[j+1] = x[j]-1
			elif x<1:
				x[j+1] = x[j]+1
			else:
				if tot_round_init>orig_init:
					orig_init=tot_round_init
					print("improved initial move count to: " + str(orig_init))
					x[j+1] = x[j]+1
				else:
					x[j+1] = x[j]-1
			tot_round_init = 0
			print("Chances of jump: " + str((float(x[j])/10.0)))

		pydirectinput.keyDown(',')
		num = random.randint(0,9)
		if num<x[j]:
			pydirectinput.keyDown('right')
			pydirectinput.keyDown('.')
			time.sleep(0.4)
			pydirectinput.keyUp('.')
			pydirectinput.keyUp('right')
		elif num>x[j] and num<7:
			pydirectinput.keyDown('right')
			time.sleep(0.5)
			pydirectinput.keyUp('right')
		elif num>=7:
			pydirectinput.press('left')
	
		init_r += str(num)
		tot_round_init += 1
		print(init_r)
	init[j]=(init_r)
